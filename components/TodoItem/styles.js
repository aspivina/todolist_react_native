import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      width: 280,
      flexDirection: 'row',
       alignItems: 'center',
       alignContent: 'center',
       justifyContent: 'space-between',
      backgroundColor: '#216583',
      borderRadius: 5,
  
  
    },
    innerText:{
        textAlign:'left',
        width: 200,


    },
  
    active:{
      color: '#f7be16',
      borderColor: '#f7be16',
      fontWeight: 'bold',


    },
    completed:{
      color: '#293462',
      borderColor: '#293462',
      textDecorationLine: 'line-through',

    },
    itemList:{
      backgroundColor: '#216583',
  
    },  
    textItem:{
      color: '#f7be16',
  
    },
    Button:{
      alignItems: 'center',
      justifyContent: 'center',
      alignItems: 'center',
   },
  
   statusButton:{
    margin: 5,
    backgroundColor: '#216583',
    fontWeight: 'bold',
    overflow: 'hidden',
    textAlign:'center',
    padding: 5,
    borderRadius: 50,
    height: 30,
    width: 30,
    borderWidth: 2,
  },  
  deleteButton:{
      margin: 5,
      backgroundColor: '#293462',
      fontWeight: 'bold',
      overflow: 'hidden',
      textAlign:'center',
      color: '#00818a',
      padding: 5,
      borderRadius: 5,
      height: 30,
   },  
  
  });
  export default styles;