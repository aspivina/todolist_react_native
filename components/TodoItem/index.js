import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text, View } from 'react-native';
import styles from './styles'

// import '../../assets/styles.css';

const TodoItem = ({ todo, onClick, onDelete }) => {
  const onClickHandler = (myId) => {
     onClick(myId)
  }
  const onDeleteHandler = (myId) => {
     onDelete(myId)
  }



  return (
    // <View className="todoItem">
    <View style={styles.container}>
        <TouchableOpacity 
          style={[styles.Button, !todo.completed ? styles.active : styles.completed ]}        
         onPress={() => onClickHandler(todo.id)}>
          <Text style={[styles.statusButton, !todo.completed ? styles.active : styles.completed ]}>V</Text>
        </TouchableOpacity>  
        <Text 
          style={[styles.innerText, !todo.completed ? styles.active : styles.completed] }
            >{todo.text}
        </Text>

        <TouchableOpacity style={styles.Button}
         onPress={() => onDeleteHandler(todo.id)}>
          <Text style={styles.deleteButton}>X</Text>
        </TouchableOpacity>        

    </View>
  );
}

TodoItem.propTypes = {
  todo: PropTypes.shape({
            id: PropTypes.number.isRequired,
            completed: PropTypes.bool.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default TodoItem