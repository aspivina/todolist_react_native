import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity,Button } from 'react-native';

const Footer = ({currentFilter, onClick}) => {

  const styles = StyleSheet.create({
    container: {
      flex: 0.15,
      flexDirection: 'row',
      backgroundColor: '#00818a',
      width: 300,
      borderRadius: 5,
      justifyContent: 'space-between',
    },
    filterButton:{
      alignItems: 'center',
      justifyContent: 'center',
      alignItems: 'center',
   },
    textButton:{
      width: 80,
      margin: 10,
      backgroundColor: '#293462',
      fontWeight: 'bold',
      overflow: 'hidden',
      textAlign:'center',
      color: '#00818a',
      padding: 5,
      borderRadius: 5,
      height: 30,
   },
   highligthed:{
      color: '#f7be16',

   },
   completed:{
    //color: '#293462',
    borderColor: '#293462',
    textDecorationLine: 'line-through',
   },
  });

  const onClickHandler = (filter) => {
     onClick(filter)
  }

  const highligthedFilterAll = (filter) => {
    switch (filter) {
      case 'SHOW_ALL':
        return true;
      default:
        return false;
    }
  };
  const highligthedFilterActive = (filter) => {
    switch (filter) {
      case 'SHOW_ACTIVE':
        return true;
      default:
        return false;
    }
  };
  const highligthedFilterCompleted = (filter) => {
    switch (filter) {
      case 'SHOW_COMPLETED':
        return true;
      default:
        return false;
    }
  };

  return (
    <View style={styles.container}>
        <TouchableOpacity style={styles.filterButton}
         onPress={() => onClickHandler('SHOW_ALL')}>
          <Text style={[styles.textButton, highligthedFilterAll(currentFilter) ? styles.highligthed : '']}>All</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filterButton}
         onPress={() => onClickHandler('SHOW_ACTIVE')}>
          <Text style={[styles.textButton, highligthedFilterActive(currentFilter) ? styles.highligthed : '']}>Active</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filterButton}
         onPress={() => onClickHandler('SHOW_COMPLETED')}>
          <Text style={[styles.textButton, highligthedFilterCompleted(currentFilter) ? styles.highligthed : '']}>Complete</Text>
        </TouchableOpacity>
    </View>

  );
}


// TodoList.propTypes = {
//   todos: PropTypes.arrayOf(
//     PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       completed: PropTypes.bool.isRequired,
//       text: PropTypes.string.isRequired
//     }).isRequired
//   ).isRequired,
//   onClick: PropTypes.func.isRequired
// }

export default Footer