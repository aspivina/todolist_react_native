import React from 'react'
import PropTypes from 'prop-types'

import { StyleSheet, ScrollView, View } from 'react-native';


import TodoItem from '../TodoItem'

const TodoList = ({ todos, filter, onClick, onDelete }) => {
  const onClickHandler = (myId) => {
     onClick(myId)
  }
  const onDeleteHandler = (myId) => {
     onDelete(myId)
  }
const getVisibleTodos = (todos, filter) => {
  console.log(filter);
  switch (filter) {
    case 'SHOW_ALL':
      return todos;
    case 'SHOW_COMPLETED':
      return todos.filter(t => t.completed);
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed);
    default:
      return todos;
  }
};
  return (
    <View style={styles.container}>
      <ScrollView >
        {getVisibleTodos(todos,filter).map(item => (
          <View style={styles.item} key={item.id} >
              <TodoItem 
                todo={item}
                onClick={() => onClickHandler(item.id)} 
                onDelete={() => onDeleteHandler(item.id)}
              />
          </View>
        ))}
      </ScrollView>

    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 300,
    borderRadius: 5,
    flexDirection: 'column',
    backgroundColor: '#00818a',
    padding: 10,
     alignItems: 'center',
    // alignContent: 'center',
    // justifyContent: 'flex-start',
  },
  item:{
    //padding: 5,
    marginBottom: 10,
  },


});
TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onClick: PropTypes.func.isRequired
}

export default TodoList