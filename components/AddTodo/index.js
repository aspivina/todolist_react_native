import React, { useState } from 'react'
import { StyleSheet,TouchableOpacity, Text,Button, TextInput,View } from 'react-native';


let AddTodo = ({onChange}) => {

  const [value,setValue] = useState(null);
  
  // debugger;
    const keyUpHandler = (v) => {
        setValue(v);
      
    }
    const onClickAdd = () => {

        // hit enter, create new item if field isn't empty
        if(value){

          onChange(value);
          setValue(null);
        }
      } 
    

    let input;
    const styles = StyleSheet.create({
      container: {
        width: 300,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00818a',

      },
      input: {
        height: 40,
        width: 230,
        backgroundColor: '#216583',
        color: '#f7be16',
        height: 40,
        margin: 10,
        padding: 5,
        borderRadius: 5,
     },
     addButton:{
      alignItems: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      marginRight: 10,

      

     },
     textButton:{
      // margin: 5,
      // backgroundColor: '#f7be16',
      // fontWeight: 'bold',
      // overflow: 'hidden',
      // textAlign:'center',
      // color: '#293462',
      // padding: 5,
      // borderRadius: 5,
       height: 40,
      width: 40,
      //margin: 10,
      backgroundColor: '#f7be16',
      fontWeight: 'bold',
      overflow: 'hidden',
      textAlign:'center',
      color: '#293462',
      //padding: 5,
      borderRadius: 5,
      textAlignVertical: 'center',


     },
      title: {
        fontSize: 19,
        fontWeight: 'bold',
      },
      activeTitle: {
        color: 'red',
      },

    });
    return (
      // className="header"
      <View style={styles.container}>
        <TextInput
          autoFocus={true}
          style={styles.input}
          placeholder="What needs to be done?"
          onChangeText={(v) => keyUpHandler(v)}
          value={value}
          clearButtonMode="always"
        />
        <TouchableOpacity style={styles.addButton} onPress={() => onClickAdd()}>
          <Text style={styles.textButton}>ADD</Text>
        </TouchableOpacity>
      </View>  
    );

  };

  export default AddTodo