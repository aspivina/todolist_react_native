import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


import AddTodo from '../../components/AddTodo/index';
import TodoList from '../../components/TodoList';
import Footer from '../../components/Footer';



const ListScreen = ({todos,filter, onAddTodo, onToggleTodo, onDeleteTodo, onVisibilityFilter}) => {
    function  Separator() {
        return <View style={styles.separator} />;
      }
    return (
       <View style={styles.container}>
           <Text style={styles.title}>-.Todo List.-</Text>
           <Separator />
           <AddTodo onChange={value => onAddTodo(value)}/>
           <Separator />
           <Footer currentFilter={filter} onClick={newFilter => onVisibilityFilter(newFilter)}/>
           <Separator />
             <TodoList   todos={todos} 
                        filter={filter}
                        onClick={id => onToggleTodo(id)}
                        onDelete={id=> onDeleteTodo(id)}/> 
        </View>


    )
};

const styles = StyleSheet.create({
    container: {
        flex: .9,
    //   borderRadius: 4,
    //   borderWidth: 0.5,
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: '#293462',
    //   borderColor: '#d6d7da',
    },
    title: {
      fontSize: 19,
      fontWeight: 'bold',
      color: '#f7be16'
    },
    activeTitle: {
      color: 'red',
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
      },
  });

export default ListScreen;


