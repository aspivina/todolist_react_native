import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Provider } from 'react-redux'
import { createStore } from 'redux';
import ListScreen from './screens/ListScreen'


import todoApp from './store/reducers'


let store = createStore(
  todoApp,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const App = () => (
  <Provider store={store}>
    <View style={styles.container}>
        <ListScreen /> 
    </View>
  </Provider> 

)

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#293462',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
